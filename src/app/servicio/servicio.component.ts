import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { CommonModule } from '@angular/common';
import { NavController } from '@ionic/angular';
import { SolicitudService } from '../services/solicitud.service';
import { UsuariosolicitudService } from '../services/usuariosolicitud.service';
import { ComunService } from '../services/comun.service';
import { delay, finalize, retry } from 'rxjs';

@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.component.html',
  styleUrls: ['./servicio.component.scss'],
})
export class ServicioComponent  implements OnInit {

  @Input() data: any[] = [];  // La lista de datos a mostrar
  nombreServicio!: string;
  idServicio!: number;
  usuarios:any[]=[];
  insertID!:number;

  constructor(private route: ActivatedRoute, private usuarioService: UsuarioService, private navCtrl: NavController,
    private solicitudService: SolicitudService, private usuarioSolicitudService: UsuariosolicitudService, 
    private comunService:ComunService  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.nombreServicio = params.get('nombre')!;
      // Ahora puedes usar 'nombreServicio' en tu componente
    });
    this.insertID = -1;
    this.cargarUsuarios();
  }

  solicitarServicio(){
    // alert('solicitar servicio!');
    let idusuario = this.comunService.idusuario;
    let idservicio = this.usuarioService.idServicio;
    this.solicitudService.newSolicitud('pendiente','nueva solicitud',idusuario,idservicio)
    // .pipe(
    //   retry(0),
    //   delay(0),
    //   finalize(() =>{
    //     console.log("id solicitud:", this.insertID);
    //     this.usuarioSolicitudService.newSolicitud(idusuario,this.insertID)
    //     .subscribe((res:any)=>{
    //       console.log("res: ",res)
    //     })
    //   })
    // )
    .subscribe((res:any)=>{
      console.log("res: ",res);
      this.usuarioSolicitudService.newSolicitud(idusuario,res.insertId)
        .subscribe((res:any)=>{
          console.log("res: ",res)
          alert('solicitud agregada correctamente!');
        })
      // this.insertID = res.insertID;
    })
  }

  volver(){
    this.navCtrl.back();
  }

  cargarUsuarios(){
    // console.log("idServicio: ", this.idServicio);
    this.usuarioService.getUsuarioByServicio(this.usuarioService.idServicio)
    .subscribe((res:any)=> {
      console.log("res: ", res);
      this.usuarios = res;
    });
  }

}
