import { CanActivateFn } from '@angular/router';
import { ComunService } from '../services/comun.service';
import { inject } from '@angular/core';


export const authGuard: CanActivateFn = (route, state) => {
  const comunService = inject(ComunService);

  if(comunService.usuarioConectado){
    return true;
  }else{
    return false;
  }
};
