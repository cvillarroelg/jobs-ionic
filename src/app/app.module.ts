import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule, NgFor } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ServicioComponent } from './servicio/servicio.component';
import {ReactiveFormsModule } from '@angular/forms';
import { EditarPerfilModalComponent } from './editar-perfil-modal/editar-perfil-modal.component';


@NgModule({
  declarations: [AppComponent, ServicioComponent, EditarPerfilModalComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [CommonModule, BrowserModule, FormsModule, ReactiveFormsModule, IonicModule.forRoot(),
     IonicModule, AppRoutingModule, NgFor,
     HttpClientModule, IonicModule.forRoot({ mode: 'ios' })],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
