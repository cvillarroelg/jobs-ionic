import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(private router: Router, private usuarioService: UsuarioService) {}

  // prueba(){
  //   alert('prueba');
  // }

  navigateToService(nombre: string, id:number) {
    this.router.navigate(['/servicio', nombre]);
    this.usuarioService.idServicio = id;
  }

}
