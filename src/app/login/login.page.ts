import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';
import { ComunService } from '../services/comun.service';
import { delay, finalize, retry } from 'rxjs';
import { ServiciosService } from '../services/servicios.service';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm!: FormGroup;
  registerForm!: FormGroup;
  loading: boolean = false;
  authMode: string = 'login';
  services: any[] = [];

  constructor(private fb: FormBuilder, private router: Router, private loginService: LoginService,
    private comunService: ComunService, private ServicioService: ServiciosService, private UsuarioService: UsuarioService) { }

  ngOnInit() {
    this.cargarServicios();
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    this.registerForm = this.fb.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      rut: ['', [Validators.required]],
      celular: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      service: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      // tpusuario:['', [Validators.required]],
      // confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    });
    console.log("services: ", this.services);
  }

  onLogin() {
    if (this.loginForm.valid) {
      this.loading = true;
      // Aquí puedes agregar la lógica para autenticar al usuario
      let correo = this.loginForm.get('email')?.value;
      let clave = this.loginForm!.get('password')?.value;
      this.loginService.iniciarSesion(correo, clave)
        .pipe(
          retry(0),
          delay(10),
          finalize(() => {
            this.loading = false;
            this.loginForm.get('email')?.setValue("");
            this.loginForm.get('password')?.setValue("");
            // console.log("navegar");
            // this.router.navigate(['']);
          })
        )
        .subscribe((res: any) => {
          console.log("res: ", res);
          if (res.length > 0) {
            this.comunService.usuarioConectado = true;
            this.comunService.idusuario = res[0].idusuario;
            this.comunService.nombre = res[0].nombre;
            this.comunService.apellido = res[0].apellido;
            this.comunService.celular = res[0].celular;
            this.comunService.rut = res[0].rut;
            this.comunService.direccion = res[0].direccion;
            this.comunService.correo = res[0].correo;
            this.router.navigate(['tabs']);
          } else {
            alert('error al ingresar, intente nuevamente');
          }
          // this.router.navigate(['']);
        });
      console.log('Formulario enviado', this.loginForm.value);

      // Redirige a otra página si el login es exitoso
      // this.router.navigate(['']);
    } else {
      alert('Formulario inválido');
    }
  }

  cargarServicios() {
    this.ServicioService.getServicios().subscribe((res: any) => {
      console.log("res: ", res);
      res.forEach((element: any) => {
        this.services.push(element);
      });
      // this.services = res;
    })
  }

  registrarse() {
    let idTPUsuario = 0;
    let service:any=null;
    if (this.registerForm.valid) {
      console.log(this.registerForm);
      if (this.registerForm.get('service')?.value == -1) {
        idTPUsuario = 2;
        // service = null;
        // alert('soy if');
      } else {
        idTPUsuario = 1;
        service = this.registerForm.get('service')?.value;
        // alert('soy else');
      }

      this.loading = true;
      let nombre = this.registerForm.get('nombre')?.value;
      let apellido = this.registerForm.get('apellido')?.value;
      let rut = this.registerForm.get('rut')?.value;
      let direccion = this.registerForm.get('direccion')?.value;
      // let service = this.registerForm.get('service')?.value;
      let celular = this.registerForm.get('celular')?.value;
      let correo = this.registerForm.get('email')?.value;
      let clave = this.registerForm!.get('password')?.value;
      // let tpusuario = 1;
      this.UsuarioService.verificarExiste(correo).subscribe((res: any) => {
        console.log("res", res);
        if (res[0].existe > 0) {//existe
          alert('usuario ya se encuentra registrado');
          window.location.reload();
        } else {//no existe
          this.UsuarioService.agregarUsuario(nombre, apellido, rut, celular, direccion, correo, clave, service, idTPUsuario)
            .subscribe((res: any) => {
              console.log("res: ", res);
              if (res.affectedRows > 0) {
                alert('Registrado correctamente!');
                // this.router.navigate(['login']);
                window.location.reload();
              } else {
                alert('error al ingresar, intente nuevamente');
              }
            });
        }
      })

      // console.log("form: ", this.registerForm);
      // console.log('Formulario enviado', this.loginForm.value);
    }
    else {
      // console.log("form: ", this.registerForm);
      alert('Formulario inválido');
    }
  }

}
