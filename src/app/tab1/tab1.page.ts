import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private router:Router, private usuarioService: UsuarioService) {}

  navigateToService(nombre: string, id:number) {
    this.router.navigate(['/servicio', nombre]);
    this.usuarioService.idServicio = id;
  }

}
