import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  URL = env.endpointURL;

  idServicio!:number;

  constructor(private http: HttpClient) { }

  getUsuarioByServicio(idServicio:number){
    return this.http.post(`${this.URL}usuarios/getUsuarioByServicio`,{idServicio:idServicio});
  }

  verificarExiste(correo:string){
    return this.http.post(`${this.URL}usuarios/verificarExiste`,{correo:correo});
  }

  setIDServicio(id:number){
    this.idServicio = id;
  }

  agregarUsuario(nombre:string, apellido:string, rut:number, celular:number, direccion:string, correo:string, clave:number, idservicio:number, idTPUsuario:number){
    return this.http.post(`${this.URL}usuarios/new`,{nombre:nombre, apellido:apellido, rut:rut, celular:celular, direccion:direccion,
      correo:correo, clave:clave, idservicio:idservicio, idTPUsuario:idTPUsuario  });
  }

  modificarUsuario(nombre:string, apellido:string, celular: number, direccion:string, idusuario:number){
    return this.http.put(`${this.URL}usuarios/modificar`,{nombre:nombre, apellido:apellido, celular:celular, direccion:direccion,
      idusuario:idusuario});
  }

}
