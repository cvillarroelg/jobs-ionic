import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UsuariosolicitudService {

  URL = env.endpointURL;

  constructor(private http: HttpClient) { }

  newSolicitud(idusuario:number, idsolicitud:number){
    return this.http.post<any>(`${this.URL}usuarioSolicitud/new`,{idusuario:idusuario, idsolicitud: idsolicitud});
  }
  
}
