import { TestBed } from '@angular/core/testing';

import { UsuariosolicitudService } from './usuariosolicitud.service';

describe('UsuariosolicitudService', () => {
  let service: UsuariosolicitudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsuariosolicitudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
