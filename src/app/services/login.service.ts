import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  URL = env.endpointURL;

  constructor(private http: HttpClient) { }

  iniciarSesion(correo:string, clave: string){
    return this.http.post<any>(`${this.URL}login/getUsuario`,{correo:correo, clave:clave});    
  }
}
