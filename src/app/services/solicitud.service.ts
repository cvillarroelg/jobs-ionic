import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SolicitudService {

  URL = env.endpointURL;

  constructor(private http: HttpClient) { }

  getAllSolicitudByUsuario(idusuario:number){
    return this.http.post<any>(`${this.URL}solicitudes/allByUsuario`,{idusuario:idusuario});
  }

  newSolicitud(estado:string, detalle:string, idusuario:number, idservicios:number){
    return this.http.post<any>(`${this.URL}solicitudes/new`,{estado:estado, detalle: detalle,
       idusuario:idusuario, idservicios:idservicios});
  }
}
