import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComunService {

  usuarioConectado: boolean = false;
  // usuarioConectado: boolean = true;
  idusuario!: number;
  nombre!: string;
  apellido!: string;
  rut!: string;
  celular!: number;
  direccion!: string;
  correo!:string;

  constructor() { }
}
