import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  
  URL = env.endpointURL;

  constructor(private http: HttpClient) { }

  getServicios(){
    return this.http.get<any>(`${this.URL}servicios/getServicios`);
  }
}
