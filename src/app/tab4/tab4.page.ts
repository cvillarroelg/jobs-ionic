import { Component, OnInit } from '@angular/core';
import { SolicitudService } from '../services/solicitud.service';
import { ComunService } from '../services/comun.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})
export class Tab4Page implements OnInit {

  servicios:any[]=[];

  constructor(private solicitudService: SolicitudService, private comunService: ComunService, private navCtrl: NavController,) {}

  ngOnInit() {
    this.cargarTabla();
  }


  cargarTabla(){
    let idusuario = this.comunService.idusuario;
    this.solicitudService.getAllSolicitudByUsuario(idusuario).subscribe((res:any)=>{
      console.log("res: ",res);
      this.servicios = res;
    })
  }

  volver(){
    this.navCtrl.back();
  }

}
