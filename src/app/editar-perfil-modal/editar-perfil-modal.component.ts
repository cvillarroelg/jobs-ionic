import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { UsuarioService } from '../services/usuario.service';
import { ComunService } from '../services/comun.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-perfil-modal',
  templateUrl: './editar-perfil-modal.component.html',
  styleUrls: ['./editar-perfil-modal.component.scss'],
})
export class EditarPerfilModalComponent  implements OnInit {

  @Input() nombre!: string;
  @Input() apellido!: string;
  @Input() telefono!: number;
  @Input() direccion!: string;
  @Input() correo!: string;

  constructor(private modalController: ModalController, private usuarioService: UsuarioService,
    private comunService: ComunService, private router: Router
  ) { }

  ngOnInit() {}

  cerrarModal() {
    this.modalController.dismiss();
  }

  guardarCambios() {
    const updatedProfile = {
      nombre: this.nombre,
      apellido: this.apellido,
      telefono: this.telefono,
      direccion: this.direccion
    };
    let idusuario = this.comunService.idusuario;
    console.log("update: ",updatedProfile);
    this.usuarioService.modificarUsuario(updatedProfile.nombre, updatedProfile.apellido,updatedProfile.telefono,
      updatedProfile.direccion, idusuario).subscribe((res:any)=>{
        console.log("res: ", res);
      })

    this.modalController.dismiss(updatedProfile);
    // window.location.reload();
    this.router.navigate(['']);
  }

}
