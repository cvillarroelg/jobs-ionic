import { Component } from '@angular/core';
import { ComunService } from '../services/comun.service';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { EditarPerfilModalComponent } from '../editar-perfil-modal/editar-perfil-modal.component';

@Component({
  selector: 'app-tab5',
  templateUrl: 'tab5.page.html',
  styleUrls: ['tab5.page.scss']
})
export class Tab5Page implements OnInit {
  
  nombre!:string;
  apellido!:string;
  celular!:number;
  direccion!:string;
  correo!:string;

  constructor(private comunService: ComunService, private router: Router, private modalController: ModalController) {}

  ngOnInit(){
    this.nombre = this.comunService.nombre;
    this.apellido = this.comunService.apellido;
    this.celular = this.comunService.celular;
    this.direccion = this.comunService.direccion;
    this.correo = this.comunService.correo;
  }

  async abrirEditarPerfil() {
    const modal = await this.modalController.create({
      component: EditarPerfilModalComponent,
      componentProps: {
        nombre: this.nombre,
        apellido: this.apellido,
        telefono: this.celular,
        direccion: this.direccion,
      }
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.nombre = data.nombre;
      this.celular = data.telefono;
      this.direccion = data.direccion;
    }
  }

  cerrarSesion(){
    this.router.navigate(['login']);
    this.comunService.usuarioConectado = false;
    this.comunService.nombre = "";
    this.comunService.apellido ="";
    this.comunService.celular = -1;
    this.comunService.direccion = "";
    this.comunService.correo = "";

  }

  

}
